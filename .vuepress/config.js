import { defaultTheme } from "@vuepress/theme-default";
import { searchPlugin } from "@vuepress/plugin-search";

export default {
  lang: "zh-CN",
  title: "tvim",
  description: "Vim-fork focused on extensibility and usability",
  head: [['link', { rel: 'icon', href: 'https://s1.ax1x.com/2022/10/12/xaAK74.png' }]],
  theme: defaultTheme({
    navbar: [
      {
        text: "GitHub",
        link: "https://github.com/realtaobo/",
      },
    ],
    logo: "https://vuejs.org/images/logo.png",
    repo: "https://gitlab.com/macondo7/tvim/",
    editLink: false,
    contributors: true,
  }),
  plugins: [
    searchPlugin({
      locales: {
        "/": {
          placeholder: "Search",
        },
      },
    }),
  ],
};
 
