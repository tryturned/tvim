---
home: true
---

# quickstart

```bash
cd ~
git clone https://gitlab.com/tryturned/tvim.git
cd tvim
cp .vimrc ~/.vimrc
mkdir -p ~/.vim/
cp -r colors/ ~/.vim/
cp -r autoload/ ~/.vim/
cp -r plugged/ ~/.vim/
cd ~/.vim/plugged
unzip plugged.zip
```

# Other

## bash

<!-- my ps1 env: `export PS1='\[\e[33m\]\u\[\] \[\]\[\e[34m\]\w\[\]\[\e[36m\]$(__git_ps1 " (%s)") $ \e[m'` -->
**Bash PS1:**  

```bash
export PS1='\[\033[32m\]\u@\[\033[35m\]\h \[\033[33m\]\w\[\033[36m\]`__git_ps1`\[\033[0m\]\n$'
```

In order to support `__git_ps1` variable, you need to use the steps below:

- curl -L https://raw.github.com/git/git/master/contrib/completion/git-prompt.sh > ~/.bash_git
- And add this to the top of your `~/.bashrc`: `source ~/.bash_git`

## zsh

Zsh `PROMPT` var set:

```zsh
autoload -U colors && colors
PROMPT="%{$fg[yellow]%}%~%{$reset_color%}%{$fg[cyan]%}$(__git_ps1 "(%s) ")%{$reset_color%}%{$fg[blue]%}%#%{$reset_color%} "
```

In order to support `__git_ps1` variable, you need to use the steps below:

- curl -L https://raw.github.com/git/git/master/contrib/completion/git-prompt.sh > ~/.zsh_git
- And add this to the top of your `~/.zshrc`: `source ~/.zsh_git`

:wink: inspired by [tvim](https://gitcode.net/qq_41345173/tvim).
